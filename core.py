#!/usr/bin/env python3

from random import SystemRandom
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import (
    TimeoutException, NoSuchElementException, WebDriverException)
from fake_useragent import UserAgent

from db import dbUp, dbGetUsersPerRole, dbGetRoles, dbDone

# Windows
# phantomjs_path = r"E:\Phantom\phantomjs-2.1.1-windows\bin\phantomjs.exe"

#

# driver = webdriver.PhantomJS(executable_path=phantomjs_path,service_args=service_args)
#link = 'https://www.reddit.com/r/thisismylifenow/comments/5tuodq/guess_im_all_clean_now'



secure_random = SystemRandom()
debug = False
dbUp()


def createDriver(proxy: str, useragent: str, windowSize: tuple) -> webdriver:
    from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
    dcap = dict(DesiredCapabilities.PHANTOMJS)
    dcap["phantomjs.page.settings.userAgent"] = (useragent)

    service_args = [
        # '--proxy=' + proxy,
        '--proxy-type=https']
    phantomjs_path = r".\phantomjs.exe"

    # driver = webdriver.Firefox()
    driver = webdriver.PhantomJS(desired_capabilities=dcap, executable_path=phantomjs_path,service_args=service_args)
    driver.set_window_size(*windowSize)
    if debug:
        print('Driver created!')
    return driver


def doLogin(username: str, password: str, driver: webdriver) -> webdriver:
    try:
        driver.get("https://www.reddit.com/login")
    except TimeoutException as error:
        if debug:
            print("Error: TimeoutException ", error)
        driver.quit()
        return 1
    except WebDriverException as error:
        if debug:
            print("Error: WebDriverException ", error)
        driver.quit()
        return 1
    sleep(5)
    User = driver.find_element_by_id("user_login")
    Pass = driver.find_element_by_id("passwd_login")
    User.send_keys(username)
    Pass.send_keys(password)
    Pass.send_keys(Keys.RETURN)
    sleep(1)
    if 'incorrect username or password' in driver.page_source:
        raise ValueError('Incorrect username or password')
    return driver


def targetUp(url: str, wait: tuple, driver: webdriver) -> bool:
    sleep(secure_random.randint(*wait))
    driver.get(url)
    sleep(secure_random.randint(*wait))
    try:
        sleep(5)
        driver.execute_script("document.querySelector('.sitetable div.arrow.up').click()")
        print('click')
        sleep(5)
    except NoSuchElementException:
        print('NoSuchElementException')
    print('OK!', url)
    return True


def randChangeVote(driver: webdriver) -> None:
    if secure_random.choice([True, False]):
        try:
            up = len(driver.find_elements_by_class_name('up'))
            button = secure_random.randint(0,up)
            driver.execute_script("document.querySelectorAll('div.arrow.up')[{0}].click()".format(button))
        except:
            pass
    else:
        try:
            down = len(driver.find_elements_by_class_name('down'))
            button = secure_random.randint(0,down)
            driver.execute_script("document.querySelectorAll('div.arrow.down')[{0}].click()".format(button))
        except:
            pass
    return


def doLikeHuman(vote: tuple, wait: tuple, driver: webdriver) -> webdriver:
    voteCount = secure_random.randint(*vote)
    if debug:
        print('voteCount: ', voteCount)
    for i in range(voteCount):
        sleep(secure_random.randint(*wait))
        for j in range(0, secure_random.randint(0, 3)):
            randChangeVote(driver)
            sleep(secure_random.randint(0, 2))
        driver.get('https://www.reddit.com/r/random')
    return driver


def doWork(user: dict, role: dict) -> None:
    if debug:
        print(user, role)
    driver = createDriver(user['proxy'], user['useragent'],  (user['windowWidth'], user['windowHeight']))
    driver = doLogin(user['username'], user['password'], driver)
    driver = doLikeHuman((role['voteFrom'], role['voteTo']), (user['waitFrom'], user['waitTo']), driver)
    print(targetUp(role['link'], (user['waitFrom'], user['waitTo']), driver))
    driver.quit()
    dbDone((user['id'], role['id']))
    return


def main():
    for role in dbGetRoles():
        for user in dbGetUsersPerRole(role['id']):
            doWork(user, role)


if __name__ == '__main__':
    debug = True
    main()
