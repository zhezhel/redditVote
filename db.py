from PyQt5.QtSql import QSqlDatabase, QSqlQuery

db = None

def exec_(query):
    q = QSqlQuery()
    q.exec_(query)

    results = []
    headers = []
    q.first()
    result = q.result()

    for i in range(q.record().count()):
        headers.append(q.record().field(i).name())

    while q.isValid():
        record = {}
        for field in headers:
            record[field] = q.value(field)
        results.append(record)
        q.next()

    return results

def dbUp():
    db = QSqlDatabase.addDatabase('QSQLITE')
    db.setDatabaseName('db.sqlite3')
    db.open()
    exec_('''
        CREATE TABLE IF NOT EXISTS users(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT NOT NULL,
        password TEXT NOT NULL,
        proxy TEXT NOT NULL,
        useragent TEXT NOT NULL,
        windowWidth INTEGER NOT NULL,
        windowHeight INTEGER NOT NULL,
        waitFrom INTEGER NOT NULL,
        waitTo INTEGER NOT NULL
    )''')

    exec_('''
        CREATE TABLE IF NOT EXISTS roles(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL,
        link TEXT NOT NULL,
        voteFrom INTEGER NOT NULL,
        voteTo INTEGER NOT NULL
    )''')

    exec_('''
        CREATE TABLE IF NOT EXISTS users_votes(
        userId INTEGER NOT NULL,
        roleId INTEGER NOT NULL,
        FOREIGN KEY(userId) REFERENCES users(id),
        FOREIGN KEY(roleId) REFERENCES roles(id)
    )''')
    return 


def dbGetRow(db:str, row:int):
    data = exec_('''
        SELECT * FROM {0} LIMIT 1 OFFSET {1};
    '''.format(db, row))
    return data

def dbDeleteRow(db:str, row:int):
    exec_('''
        DELETE FROM {0} WHERE id=={1};
    '''.format(db, row))
    return

def dbGetUsersPerRole(roleId: int):
    role = (roleId,)
    data = exec_('''
        SELECT * FROM users WHERE id NOT IN (SELECT userId FROM users_votes WHERE roleId={})
    '''.format(role))
    return data


def dbGetUsersPerRole(roleId: int):
    data = exec_('''
        SELECT * FROM users WHERE id NOT IN (SELECT userId FROM users_votes WHERE roleId={})
    '''.format(roleId))
    return data


def dbGetRoles():
    data = exec_('''
        SELECT * FROM roles;
    ''')
    return data


def dbDone(userRole:tuple) -> None:
    print(userRole)
    exec_('''
        INSERT INTO users_votes (userId, roleId) VALUES ({}, {})
    '''.format(userRole[0], userRole[1]))
    return

