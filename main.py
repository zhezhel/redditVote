#!/usr/bin/env python3
import subprocess
import sys
from PyQt5.QtCore import QPoint
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.QtSql import QSqlQueryModel
from PyQt5.QtCore import QObject, pyqtSignal, QCoreApplication
from ui.gui import Ui_Gui
from ui.createUser import Ui_CreateUser
from ui.createRole import Ui_CreateRole
from db import dbUp, exec_, dbGetRow, dbDeleteRow
from fake_useragent import UserAgent
ua = UserAgent()
dbUp()


class EditRole(Ui_CreateRole, QObject):
    hidden = pyqtSignal()
    id_ = 0

    def __init__(self):
        Ui_CreateRole.__init__(self)
        QObject.__init__(self)
        self.dialog = QDialog()
        self.dialog.setModal(True)
        self.setupUi(self.dialog)
        self.closeBtn.clicked.connect(self.hide)
        self.addBtn.clicked.connect(self.add)

    def show(self, row):
        self.id_ = row['id']
        self.lineEdit.setText(row['name'])
        self.lineEdit_2.setText(row['link'])
        self.lineEdit_3.setText(str(row['voteFrom']))
        self.lineEdit_4.setText(str(row['voteTo']))
        self.dialog.show()

    def hide(self):
        self.dialog.hide()
        self.hidden.emit()
  
    def add(self):
        exec_('''
            UPDATE roles SET 
            name='{0}', link='{1}', voteFrom={2}, voteTo={3} WHERE id=={4}
            '''.format(
            self.lineEdit.text(), self.lineEdit_2.text(), self.lineEdit_3.text(),
            self.lineEdit_4.text(), self.id_)
        )
        self.hide()


class CreateRole(Ui_CreateRole, QObject):
    hidden = pyqtSignal()

    def __init__(self):
        Ui_CreateRole.__init__(self)
        QObject.__init__(self)
        self.dialog = QDialog()
        self.dialog.setModal(True)
        self.setupUi(self.dialog)
        self.closeBtn.clicked.connect(self.hide)
        self.addBtn.clicked.connect(self.add)

    def show(self):
        self.dialog.show()

    def hide(self):
        self.dialog.hide()
        self.hidden.emit()
  
    def add(self):
        exec_('''
            INSERT INTO roles (name, link, voteFrom, voteTo) VALUES (
            '{0}', '{1}', {2}, {3})'''.format(
            self.lineEdit.text(), self.lineEdit_2.text(), self.lineEdit_3.text(),
            self.lineEdit_4.text())
        )
        self.hide()


class EditUser(Ui_CreateUser, QObject):
    hidden = pyqtSignal()
    id_ = 0

    def __init__(self):
        Ui_CreateUser.__init__(self)
        QObject.__init__(self)
        self.dialog = QDialog()
        self.dialog.setModal(True)
        self.setupUi(self.dialog)
        self.closeBtn.clicked.connect(self.hide)
        self.addBtn.clicked.connect(self.add)

    def add(self):
        exec_('''
            UPDATE users SET
            username='{0}', password='{1}', proxy='{2}', useragent='{3}',
            windowWidth={4}, windowHeight={5}, waitFrom={6}, waitTo={7}
            WHERE id=={8}'''.format(
            self.usernameEdit.text(), self.passwordEdit.text(),
            self.proxyEdit.text() + ':' + str(self.portEdit.value()),
            ua[self.browserSelect.currentText()], self.windowWidth.text(),
            self.windowHeight.text(), self.waitFrom.text(), self.waitTo.text(), self.id_)
        )
        self.hide()

    def show(self, row): 
        self.id_ = row['id']
        self.usernameEdit.setText(row['username'])
        self.passwordEdit.setText(row['password'])
        self.proxyEdit.setText(row['proxy'].split(':')[0])

        self.portEdit.setValue(int(row['proxy'].split(':')[1]))
        
        self.windowWidth.setValue(row['windowWidth'])
        self.windowHeight.setValue(row['windowHeight'])
        self.waitFrom.setValue(row['waitFrom'])
        self.waitTo.setValue(row['waitTo'])
        self.dialog.show()

    def hide(self):
        self.dialog.hide()
        self.hidden.emit();


class CreateUser(Ui_CreateUser, QObject):
    hidden = pyqtSignal()

    def __init__(self):
        Ui_CreateUser.__init__(self)
        QObject.__init__(self)
        self.dialog = QDialog()
        self.dialog.setModal(True)
        self.setupUi(self.dialog)
        self.closeBtn.clicked.connect(self.hide)
        self.addBtn.clicked.connect(self.add)

    def add(self):
        exec_('''
            INSERT INTO users (username, password, proxy, useragent,
            windowWidth, windowHeight, waitFrom, waitTo) VALUES (
            '{0}', '{1}', '{2}', '{3}', {4}, {5}, {6}, {7})'''.format(
            self.usernameEdit.text(), self.passwordEdit.text(),
            self.proxyEdit.text() + ':' + str(self.portEdit.value()),
            ua[self.browserSelect.currentText()], self.windowWidth.text(),
            self.windowHeight.text(), self.waitFrom.text(), self.waitTo.text())
        )
        self.hide()

    def show(self): 
        self.dialog.show()

    def hide(self):
        self.dialog.hide()
        self.hidden.emit();


class Gui(Ui_Gui):
    started = False

    def __init__(self, dialog):
        Ui_Gui.__init__(self)
        self.setupUi(dialog)
        
        self.pid = 0
        self.addNewUserDialog = CreateUser()
        self.addNewRoleDialog = CreateRole()
        self.editRoleDialog = EditRole()
        self.editUserDialog = EditUser()
        self.rolesTable.doubleClicked.connect(self.editRole)
        self.usersTable.doubleClicked.connect(self.editUser)
        self.addNewUserDialog.hidden.connect(self.loadFromDb)
        self.addNewRoleDialog.hidden.connect(self.loadFromDb)
        self.editRoleDialog.hidden.connect(self.loadFromDb)
        self.editUserDialog.hidden.connect(self.loadFromDb)
        self.addNewUserBtn.clicked.connect(self.addNewUser)
        self.addNewRoleBtn.clicked.connect(self.addNewRole)
        self.loadFromDb()
        #Roles
        self.pushButton_20.clicked.connect(self.roleDelete)
        #Users
        self.pushButton_3.clicked.connect(self.userDelete)
        self.pushButton_5.clicked.connect(self.editUser)
        self.pushButton_19.clicked.connect(self.editRole)

        self.pushButton_21.clicked.connect(self.startStop)

    def startStop(self):
        self.started = not self.started
        self.pushButton_21.setText('Stop' if self.started else 'Start')
        
        if self.started:
            self.p = subprocess.Popen(['core.exe'], shell=True)
            self.loadFromDb()
        else:
            subprocess.call(['taskkill', '/F', '/T', '/PID', str(self.p.pid)], shell=True)

    def addNewUser(self):
        self.addNewUserDialog.show()

    def addNewRole(self):
        self.addNewRoleDialog.show()

    def editRole(self):
        try:
            row = dbGetRow('roles',self.rolesTable.selectedIndexes()[0].row())[0]
            self.editRoleDialog.show(row)
        except:
            pass

    def editUser(self):
        try:
            row = dbGetRow('users',self.usersTable.selectedIndexes()[0].row())[0]
            self.editUserDialog.show(row)
        except:
            pass


    def loadFromDb(self):
        usersModel = QSqlQueryModel()
        usersModel.setQuery('''SELECT id,
            username as "Username",
            password as "Password",
            proxy as "Proxy",
            useragent as "Useragent"
        FROM users''')
        self.usersTable.setModel(usersModel)
        self.usersTable.setColumnHidden(0, True)

        rolesModel = QSqlQueryModel()
        rolesModel.setQuery('''SELECT id,
            name as "Name",
            (SELECT
                ((SELECT COUNT(*) FROM users_votes WHERE roleId = roles.id)
                /
                (SELECT COUNT(*) * 1.0 FROM users)
                * 100) || '%'
            ) as "Progress",
            voteFrom || ' → ' || voteTo as "Vote",
            link as "Link"
        FROM roles''')
        self.rolesTable.setModel(rolesModel)
        self.rolesTable.setColumnHidden(0, True)
        if self.started:
            QTimer.singleShot(1000, self.loadFromDb)

    def userDelete(self):
        try:
            id_ = dbGetRow('users',self.usersTable.selectedIndexes()[0].row())[0]['id']
            dbDeleteRow('users', id_)
            self.loadFromDb()
        except:
            pass

    def roleDelete(self):
        try:
            id_ = dbGetRow('roles',self.rolesTable.selectedIndexes()[0].row())[0]['id']
            dbDeleteRow('roles', id_)
            self.loadFromDb()
        except:
            pass


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = QDialog()
    gui = Gui(window)
    window.show()
    sys.exit(app.exec_())
