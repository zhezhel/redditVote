# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './createRole.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CreateRole(object):
    def setupUi(self, CreateRole):
        CreateRole.setObjectName("CreateRole")
        CreateRole.resize(640, 230)
        self.label_2 = QtWidgets.QLabel(CreateRole)
        self.label_2.setGeometry(QtCore.QRect(20, 90, 141, 20))
        self.label_2.setObjectName("label_2")
        self.lineEdit_3 = QtWidgets.QLineEdit(CreateRole)
        self.lineEdit_3.setGeometry(QtCore.QRect(20, 170, 101, 32))
        self.lineEdit_3.setText("")
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.closeBtn = QtWidgets.QPushButton(CreateRole)
        self.closeBtn.setGeometry(QtCore.QRect(530, 90, 96, 32))
        self.closeBtn.setObjectName("closeBtn")
        self.label = QtWidgets.QLabel(CreateRole)
        self.label.setGeometry(QtCore.QRect(20, 30, 141, 20))
        self.label.setObjectName("label")
        self.lineEdit_4 = QtWidgets.QLineEdit(CreateRole)
        self.lineEdit_4.setGeometry(QtCore.QRect(180, 170, 101, 32))
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.lineEdit = QtWidgets.QLineEdit(CreateRole)
        self.lineEdit.setGeometry(QtCore.QRect(20, 50, 471, 32))
        self.lineEdit.setText("")
        self.lineEdit.setObjectName("lineEdit")
        self.addBtn = QtWidgets.QPushButton(CreateRole)
        self.addBtn.setGeometry(QtCore.QRect(530, 50, 96, 32))
        self.addBtn.setObjectName("addBtn")
        self.lineEdit_2 = QtWidgets.QLineEdit(CreateRole)
        self.lineEdit_2.setGeometry(QtCore.QRect(20, 110, 471, 32))
        self.lineEdit_2.setText("")
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.label_3 = QtWidgets.QLabel(CreateRole)
        self.label_3.setGeometry(QtCore.QRect(20, 150, 141, 20))
        self.label_3.setObjectName("label_3")
        self.label_7 = QtWidgets.QLabel(CreateRole)
        self.label_7.setGeometry(QtCore.QRect(140, 170, 21, 31))
        self.label_7.setObjectName("label_7")

        self.retranslateUi(CreateRole)
        QtCore.QMetaObject.connectSlotsByName(CreateRole)

    def retranslateUi(self, CreateRole):
        _translate = QtCore.QCoreApplication.translate
        CreateRole.setWindowTitle(_translate("CreateRole", "Dialog"))
        self.label_2.setText(_translate("CreateRole", "Link"))
        self.closeBtn.setText(_translate("CreateRole", "Close"))
        self.label.setText(_translate("CreateRole", "Name"))
        self.addBtn.setText(_translate("CreateRole", "Add"))
        self.label_3.setText(_translate("CreateRole", "Upvote"))
        self.label_7.setText(_translate("CreateRole", "to"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CreateRole = QtWidgets.QDialog()
    ui = Ui_CreateRole()
    ui.setupUi(CreateRole)
    CreateRole.show()
    sys.exit(app.exec_())

