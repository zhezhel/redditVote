#!/bin/sh

for file in $(dirname $0)/*.ui; do
  name="$(dirname $0)/${file%.*}"
  pyuic5 -o $name.py -x $file;
done

